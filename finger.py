# Claudio Freda (123456) & Kevin Ouwehand (6031919)
# Middle finger assignment for Motion & Manipulation

import time
import random
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from math import *
from pylab import *
from itertools import chain
from matplotlib.patches import Rectangle
from matplotlib.widgets import Slider, Button, RadioButtons
from sympy.utilities.lambdify import lambdify



################################################################################
# Some config variables
################################################################################
CONSTRAINED_RATIO = 2.0 / 3.0 			# theta_D = CONSTRAINED_RATIO * theta_P

MIN_ANGLE_THETA_M = -pi / 3.0 			# - pi / 3 <= theta_M <= pi / 3
MAX_ANGLE_THETA_M = pi / 3.0

MIN_ANGLE_THETA_PD = -2.0 * pi / 3.0 	# -2pi / 3 <= theta_P <= 0   and
MAX_ANGLE_THETA_PD = 0 					# -2pi / 3 <= theta_D <= 0

PROXIMAL_LINK = 44.6 					# Lengths for the finger links
INTERMEDIATE_LINK = 26.3
DISTAL_LINK = 17.4



################################################################################
# Class for the Interaction
################################################################################
class Interaction(object):
	def __init__(self, finger, solver, figure, ax):
		super(Interaction, self).__init__()
		self.finger = finger
		self.solver = solver
		self.targetPosition = np.matrix([70, 10]).T		# Placeholder
		self.figure, self.ax, self.ax2 = figure, ax[0], ax[1]
		self.cid = self.figure.canvas.mpl_connect("button_press_event", self.setNewGoalPosition)

		# Add sliders for the angles theta M, P, D
		axThetaM = plt.axes([0.25, 0.27, 0.65, 0.03])
		axThetaP = plt.axes([0.25, 0.22, 0.65, 0.03])
		axThetaD = plt.axes([0.25, 0.17, 0.65, 0.03])

		self.sliderThetaM = Slider(axThetaM, "Theta M * pi", MIN_ANGLE_THETA_M / pi,
			MAX_ANGLE_THETA_M / pi, valinit = 0)
		self.sliderThetaP = Slider(axThetaP, "Theta P * pi", MIN_ANGLE_THETA_PD / pi,
			MAX_ANGLE_THETA_PD / pi, valinit = 0)
		self.sliderThetaD = Slider(axThetaD, "Theta D * pi", MIN_ANGLE_THETA_PD / pi,
			MAX_ANGLE_THETA_PD / pi, valinit = 0)

		# Set the callbacks for each
		self.sliderThetaM.on_changed(self.updateThetaM)
		self.sliderThetaP.on_changed(self.updateThetaP)
		self.sliderThetaD.on_changed(self.updateThetaD)

		# Add a reset button
		resetAx = plt.axes([0.75, 0.12, 0.15, 0.03])
		self.resetButton = Button(resetAx, "Reset", color="Orange", hovercolor="0.975")
		self.resetButton.on_clicked(self.resetEverything)

		# Add a goto position/solve button
		solveAx = plt.axes([0.25, 0.12, 0.15, 0.03])
		self.solveButton = Button(solveAx, "Solve", color="Green", hovercolor="0.975")
		self.solveButton.on_clicked(self.solveIterative)

		# Add a random orientation button
		randomAx = plt.axes([0.425, 0.12, 0.30, 0.03])
		self.randomButton = Button(randomAx, "Random orientation", color="Green", hovercolor="0.975")
		self.randomButton.on_clicked(self.setRandomAngles)

		# Add a slide button
		slideAx = plt.axes([0.25, 0.07, 0.15, 0.03])
		self.slideButton = Button(slideAx, "Slide", color="Green", hovercolor="0.975")
		self.slideButton.on_clicked(self.slideAlongO)

		# Add a quit button
		quitAx = plt.axes([0.75, 0.07, 0.15, 0.03])
		self.quitButton = Button(quitAx, "Quit", color="Red", hovercolor="0.975")
		self.quitButton.on_clicked(self.quit)

		# Add a configuration space sampling button
		configSpaceAx = plt.axes([0.425, 0.07, 0.30, 0.03])
		self.configurationSpaceButton = Button(configSpaceAx, "Draw Configuration Space",
			color="Orange", hovercolor="0.975")
		self.configurationSpaceButton.on_clicked(self.drawConfigurationSpace)

		# And finally we collect all the axes... see trackMouseMovement function
		self.uiAxes = [axThetaM, axThetaP, axThetaD, resetAx, solveAx, randomAx,
			slideAx, quitAx, configSpaceAx, self.ax2]

		self.resetEverything()

	def __del__(self):
		# Disable the mouse tracking
		if (hasattr(self, "cid") and hasattr(self, "figure")):
			self.figure.canvas.mpl_disconnect(self.cid)

	def updateThetaM(self, newAngle):
		self.finger.setAngle(0, newAngle * pi)
		self.finger.drawFinger()

	def updateThetaP(self, newAngle):
		self.finger.setAngle(1, newAngle * pi)
		self.finger.drawFinger()

	def updateThetaD(self, newAngle):
		self.finger.setAngle(2, newAngle * pi)
		self.finger.drawFinger()

	def resetEverything(self, event = None):
		self.finger.resetAngles()
		self.sliderThetaM.reset()
		self.sliderThetaP.reset()
		self.sliderThetaD.reset()
		self.setupErrorPlot()
		self.drawTargetPosition()

	def drawTargetPosition(self):
		drawPoint(self.targetPosition, "Goal", self.finger.ax)

	def setRandomAngles(self, event):
		# Scale the uniform random value from [0, 1] to the min/max angle values
		scaling = lambda x, minValue, maxValue: x * (maxValue - minValue) + minValue
		newAngles = [	scaling(np.random.rand(), MIN_ANGLE_THETA_M, MAX_ANGLE_THETA_M),
						scaling(np.random.rand(), MIN_ANGLE_THETA_PD, MAX_ANGLE_THETA_PD),
						scaling(np.random.rand(), MIN_ANGLE_THETA_PD, MAX_ANGLE_THETA_PD) ]
		self.finger.setAllAngles(newAngles)
		self.finger.drawFinger()
		self.updateSliderValues(newAngles)
		self.drawTargetPosition()

	def slideAlongO(self, event):
		# Sample from x = 120 to x = -20 with a certain step size
		STEP_SIZE = 2

		for i in range(-20, 100, STEP_SIZE):
			self.targetPosition = np.matrix([i, -20]).T
			self.solveIterative(None, False)	# Dont update substeps...

	def updateSliderValues(self, angles):
		self.sliderThetaM.set_val(angles[0] / pi)
		self.sliderThetaP.set_val(angles[1] / pi)
		self.sliderThetaD.set_val(angles[2] / pi)

	def drawConfigurationSpace(self, event):
		for theta1 in np.arange(MIN_ANGLE_THETA_M, MAX_ANGLE_THETA_M, radians(10)):
			for theta2 in np.arange(MIN_ANGLE_THETA_PD, MAX_ANGLE_THETA_PD, radians(10)):
				point = self.solver.getFingerTipPosition(theta1, theta2)

				self.ax.plot(*point.flat, marker="o", markerfacecolor='blue',
					label = "Configuration Space")
				# print point

		self.drawTargetPosition()

	def quit(self, event):
		exit()

	def setupErrorPlot(self):
		self.ax2.cla()
		self.ax2.set_xlabel("Number of iterations")
		self.ax2.set_ylabel("Absolute error E")

	def solveIterative(self, event = None, updatePerStep = True):
		# Solve iteratively with max error 1.0, and 10 steps maximum
		maxError, maxSteps = 0.1, 20

		# Always use the current angle values as the initial guess
		success, errors = True, []

		if (updatePerStep):
			success, errors = self.solver.solveIterative(self.targetPosition, None,
				maxError, maxSteps, self.drawTargetPosition)
		else:
			success, errors = self.solver.solveIterative(self.targetPosition, None,
				maxError, maxSteps, None)

		# Use the fingers current angles to update the sliders and figure
		self.finger.drawFinger()
		self.updateSliderValues(self.finger.angles)
		self.drawTargetPosition()

		# Plot the results of the (number of) iterations vs the error
		self.ax2.cla()
		self.setupErrorPlot()
		self.ax2.plot(np.arange(0, len(errors)), errors, "b-o", label="Iterations vs Error")
		plt.draw()
		plt.show(block=False)
		plt.pause(0.001)

	def setNewGoalPosition(self, event):
		"""Tracks the position of the mouse in the figure as a goal movement"""
		# Extract the x and y coordinates in the figure frame
		x, y = event.xdata, event.ydata

		# If left mouse button is pressed, use that position as a goal for the
		# inverse kinematics solver, so next time the solve button will use it.
		# Also, make sure that we only handle mouse click events that are done
		# in the plot/figure frame/axes, and not in our ui (sliders, buttons, etc)
		if (event.button == 1 and x != None and y != None and event.inaxes not in self.uiAxes):
			self.targetPosition = np.matrix([x, y]).T

			# Redraw everything and show a red dot where the user clicked
			self.finger.drawFinger()
			self.drawTargetPosition()
			self.figure.canvas.draw()



################################################################################
# Finger class
################################################################################
class Finger(object):
	"""Finger class for handling a finger"""
	angles  = [0, 0, 0]	# theta for M, P, and D, if all 0 finger is aligned with x-axis
	lengths = [0, 0, 0]	# translations for the finger lengths
	anglesAreConstrained = False
	MAX_JOINTS = 3
	MAX_LINKS = 3

	def __init__(self, fingerLengths, figure, ax, anglesAreConstrained = True):
		super(Finger, self).__init__()
		self.anglesAreConstrained = anglesAreConstrained
		self.figure = figure
		self.ax = ax
		self.lengths = fingerLengths

	def isValidAngle(self, index, angle):
		"""Returns True/False indicating whether the given angle is a valid angle"""
		# We do need to adhere to the unconstrained joint angles at least...
		if (index == 0):
			if (not (MIN_ANGLE_THETA_M <= angle and angle <= MAX_ANGLE_THETA_M)):
				return False

		if (index == 1 or index == 2):
			if (not (MIN_ANGLE_THETA_PD <= angle and angle <= MAX_ANGLE_THETA_PD)):
				return False

		if (index < 0 or index > self.MAX_JOINTS):
			return False

		# Also, check the other constraint between joint angles...
		if (self.anglesAreConstrained and index == 2):
			if (angle != CONSTRAINED_RATIO * self.angles[1]):
				return False

		# All valid!
		return True

	def getMaxBoundAngle(self, index, angle):
		"""Returns the angle bounded by the minimum/maximum value the angle can
		have according to the constraints"""

		if (index == 0):
			return max(min(angle, MAX_ANGLE_THETA_M), MIN_ANGLE_THETA_M)

		if (index == 1 or index == 2):
			return max(min(angle, MAX_ANGLE_THETA_PD), MIN_ANGLE_THETA_PD)

		# Invalid index!
		return None

	def setAngle(self, index, angle, useMaxBoundAngle = True):
		"""Sets the angle for one of the joins, with respect to the
		(un)constrained joint angles. Returns True/False indicating valid/success."""
		if (not self.isValidAngle(index, angle)):
			if (useMaxBoundAngle):
				angle = self.getMaxBoundAngle(index, angle)
			else:
				return False

		# All correct, lets set the angle
		self.angles[index] = angle
		return True

	def setAllAngles(self, angles, useMaxBoundAngle = True):
		"""Simple wrapper that sets all MAX_JOINTS angles via the setAngle
		function, returns True/False indicating valid/success for all angles."""
		allSuccess = True

		for i in range(0, Finger.MAX_JOINTS):
			if not (self.setAngle(i, angles[i], useMaxBoundAngle)):
				allSuccess = False

		return allSuccess

	def resetAngles(self, reDraw = True):
		"""Resets all the angles back to 0"""
		self.angles = [0, 0, 0]

		if (reDraw):
			self.drawFinger()

	def calculateFingerPositions(self):
		"""Returns a list with 3 elements that holds the positions of the end
		of each link, in order: proximal, intermediate, finger tip"""

		# Get all the matrices resembling the transformations
		metaMatrix = getTransformationMatrix(self.angles[0], self.lengths[0], 0)
		proximalMatrix = getTransformationMatrix(self.angles[1], self.lengths[1], 0)
		distalMatrix = getTransformationMatrix(self.angles[2], self.lengths[2], 0)
		finalMatrix = metaMatrix * proximalMatrix * distalMatrix

		proximalLinkPosition = metaMatrix * np.matrix([0, 0, 1]).T
		intermediateLinkPosition = metaMatrix * proximalMatrix * np.matrix([0, 0, 1]).T
		finalFingerTipPosition = finalMatrix * np.matrix([0, 0, 1]).T

		# And return a list of vectors with the positions of the joints/tip
		return [proximalLinkPosition, intermediateLinkPosition, finalFingerTipPosition]

	def setupPlot(self):
		"""Sets up basic plot stuff"""
		self.ax.set_title("Middle Finger")
		self.ax.set_xlabel("x-axis (mm)")
		self.ax.set_ylabel("y-axis (mm)")
		self.ax.axis("equal")
		self.ax.set_xlim(-200, 200)
		self.ax.set_ylim(-100, 100)

	def drawGround(self):
		"""Draws the ground at y = -20"""
		self.ax.add_patch(Rectangle((-20, -20), 120, -50, alpha = 0.6,
			facecolor = "red", hatch = "/"))

	def drawFinger(self):
		"""Draws the entire finger on the matplotlib screen"""
		# Get the positions of all the tips of the links
		[proximal, intermediate, fingerTip] = self.calculateFingerPositions()
		# Get the intermediate line segments resembling the finger links
		startX = [0.0, float(proximal.item(0)), float(intermediate.item(0))]
		startY = [0.0, float(proximal.item(1)), float(intermediate.item(1))]
		finishX = [float(proximal.item(0)), float(intermediate.item(0)), float(fingerTip.item(0))]
		finishY = [float(proximal.item(1)), float(intermediate.item(1)), float(fingerTip.item(1))]

		# Clear the previous plot...
		self.ax.cla()

		# Plot all the line segments resembling the finger, and the ground
		self.setupPlot()
		self.ax.plot(startX, startY, finishX, finishY, marker = "o")
		self.drawGround()
		self.figure.canvas.draw()
		plt.pause(0.001)



################################################################################
# Class for the Inverse Kinematics Solver
################################################################################
class InverseKinematicsSolver(object):
	def __init__(self, finger):
		super(InverseKinematicsSolver, self).__init__()
		self.finger = finger

		# Setup solver
		# x, y, z = sp.symbols("x y z")
		# metaMatrix = getSPTransformationMatrix(x, PROXIMAL_LINK, 0)
		# proximalMatrix = getSPTransformationMatrix(y, INTERMEDIATE_LINK, 0)
		# distalMatrix = getSPTransformationMatrix(z, DISTAL_LINK, 0)
		# finalMatrix = metaMatrix * proximalMatrix * distalMatrix

		# We only have to use 2 angles, as the third is a constrained ratio of the 2nd
		# self.finalMatrix = finalMatrix.subs(z, CONSTRAINED_RATIO * y)
		# fingerTip = self.finalMatrix * sp.Matrix([0, 0, 1])

		# self.functions = sp.Matrix(fingerTip[0:2])
		# print "Functions = " + str(self.functions)
		# self.vars = (x, y)
		# self.jacobian = self.functions.jacobian(self.vars)
		# print "Jacobian = " + str(self.jacobian)

		#array2mat = [{"ImmutableMatrix": np.matrix}, "numpy"]
		#self.getFingerTipPosition = lambdify(self.vars, self.functions, modules=array2mat)
		#self.getJacobian = lambdify(self.vars, self.jacobian, modules=array2mat)

	def getFingerParameters(self, parameters):
		"""Returns the 3 finger parameters for use with the Finger class"""
		return [parameters.flat[0], parameters.flat[1], CONSTRAINED_RATIO * parameters.flat[1]]

	def ensureNonZeroParameters(self, parameters, offset = radians(5)):
		"""Makes sure that all the parameters are nonzero by adding the small offset"""
		# Check if the parameters are (close to) 0 ...
		if (abs(parameters[0]) < offset):
			parameters[0] = -offset

		if (abs(parameters[1]) < offset):
			parameters[1] = -offset

		return parameters

	def getFingerTipPosition(self, thetaM, thetaP):
		"""Returns the position of the fingertip."""
		sin_x = sin(thetaM)
		sin_y = sin(thetaP)
		sin_z = sin(thetaP * CONSTRAINED_RATIO)
		cos_x = cos(thetaM)
		cos_y = cos(thetaP)
		cos_z = cos(thetaP * CONSTRAINED_RATIO)

		return np.matrix([
			[DISTAL_LINK * (-sin_x * sin_y + cos_x * cos_y) * cos_z +
				DISTAL_LINK * (-sin_x * cos_y - sin_y * cos_x) * sin_z -
				INTERMEDIATE_LINK * sin_x * sin_y + INTERMEDIATE_LINK * cos_x * cos_y +
				PROXIMAL_LINK * cos_x],
			[DISTAL_LINK * (-sin_x * sin_y + cos_x * cos_y) * sin_z +
				DISTAL_LINK * (sin_x * cos_y + sin_y * cos_x) * cos_z +
				INTERMEDIATE_LINK * sin_x * cos_y + PROXIMAL_LINK * sin_x +
				INTERMEDIATE_LINK * sin_y * cos_x]])

	def getJacobian(self, x, y):
		"""Returns a 2x2 jacobian matrix of the given x and y"""
		sin_x = sin(x)
		sin_y = sin(y)
		sin_z = sin(y * CONSTRAINED_RATIO)
		cos_x = cos(x)
		cos_y = cos(y)
		cos_z = cos(y * CONSTRAINED_RATIO)

		MAGIC_1 = -11.6
		MAGIC_2 = 29.0

		return np.matrix([
			[DISTAL_LINK * (sin_x * sin_y - cos_x * cos_y) * sin_z +
				DISTAL_LINK * (-sin_x * cos_y - sin_y * cos_x) * cos_z -
				INTERMEDIATE_LINK * sin_x * cos_y - PROXIMAL_LINK * sin_x -
				INTERMEDIATE_LINK * sin_y * cos_x,
			MAGIC_1 * (-sin_x * sin_y + cos_x * cos_y) * sin_z +
			 	DISTAL_LINK * (sin_x * sin_y - cos_x * cos_y) * sin_z +
			 	MAGIC_2 * (-sin_x * cos_y - sin_y * cos_x) * cos_z -
			 	INTERMEDIATE_LINK * sin_x * cos_y - INTERMEDIATE_LINK * sin_y * cos_x
			],
			[DISTAL_LINK * (-sin_x * sin_y + cos_x * cos_y) * cos_z +
				DISTAL_LINK * (-sin_x * cos_y - sin_y * cos_x) * sin_z -
				INTERMEDIATE_LINK * sin_x * sin_y + INTERMEDIATE_LINK * cos_x * cos_y +
				PROXIMAL_LINK * cos_x,
			MAGIC_2 * (-sin_x * sin_y + cos_x * cos_y) * cos_z +
				DISTAL_LINK * (-sin_x * cos_y - sin_y * cos_x) * sin_z +
				MAGIC_1 * (sin_x * cos_y + sin_y * cos_x) * sin_z -
				INTERMEDIATE_LINK * sin_x * sin_y + INTERMEDIATE_LINK * cos_x * cos_y
			]])

	def solveIterative(self, goal, initialValues = None, stopError = 0.1,
			maxSteps = 20, drawTarget = None):
		"""Attempts to use an inverse kinematics solver to iteratively determine
		the parameters yielding the goal position by minimizing the absolute error
		of the goal position and the current intermediate position. This function
		uses the Jacobian matrix and its (pseudo)inverse to calculate the
		parameter adjustments."""

		# Initial guess...
		parameters = initialValues

		# If not given, use the current parameters
		if (initialValues == None):
			parameters = np.matrix([self.finger.angles[0], self.finger.angles[1]]).T

		fingerParameters = self.getFingerParameters(parameters)

		#parameters = self.ensureNonZeroParameters(parameters)
		numSteps = absError = 0

		# Get the current position of the fingertip without homogenous element
		fingerTipPosition = self.getFingerTipPosition(*parameters.flat)

		# print "Attempting to get to target (" + str(float(goal[0])) + ", " + str(float(goal[1])) + \
		# 	") from initial position (" + str(float(fingerTipPosition[0])) + ", " + \
		# 	str(float(fingerTipPosition[1])) + ")"

		# For keeping track of success and the (number of) iterations to error ratios
		success = True
		errors = []

		# Solve iteratively by trying to minimize the absolute error
		while True:
			error = goal - fingerTipPosition
			absError = sqrt(sum(i**2 for i in error))
			errors.append(float(absError))
			self.finger.setAllAngles(fingerParameters)

			if (drawTarget != None):
				# Show the finger updates on the screen
				self.finger.drawFinger()
				drawTarget()
				plt.show(block=False)
				plt.draw()

			if absError < stopError:
				# print "Found solution in " + str(numSteps) + " steps"
				success = True
				break

			if numSteps >= maxSteps:
				# print "Not found! in " + str(numSteps) + " steps"
				success = False
				break



			# Calculate the jacobian for the current parameters of the fingerTipPosition
			evalJacobian = self.getJacobian(*parameters.flat)

			try:
				correction = np.linalg.pinv(evalJacobian) * error

			except np.linalg.linalg.LinAlgError:
				print "Determinant is 0 after " + str(numSteps) + " steps"
				success = False
				break

			# Update the new parameters with the correction
			parameters = parameters + correction
			parameters = np.matrix([angleWrap(item) for item in parameters.flat]).T
			#parameters = self.ensureNonZeroParameters(parameters)

			# And clip them to their minimum and maximum values (their bounds)
			parameters[0] = np.clip(parameters[0], MIN_ANGLE_THETA_M,  MAX_ANGLE_THETA_M)
			parameters[1] = np.clip(parameters[1], MIN_ANGLE_THETA_PD, MAX_ANGLE_THETA_PD)

			# Get the new fingertip position and update the stats
			fingerTipPosition = self.getFingerTipPosition(*parameters.flat)
			fingerParameters = self.getFingerParameters(parameters)
			numSteps += 1
			#print "Iteration: "+ str(numSteps)

		# print "Parameters found after " + str(numSteps) + " steps = [" + \
		# 	str(float(parameters[0])) + ", " + str(float(parameters[1])) + ", " + \
		# 	str(float(fingerParameters[2])) + "]"

		# print "Final fingertip position = (" + str(float(fingerTipPosition[0])) + ", " + \
		# 	str(float(fingerTipPosition[1])) + ") with final error of " + str(float(absError))
		return success, errors



################################################################################
# Utility functions
################################################################################

def getSPRotationMatrix(angle):
	"""Returns a homogenous "2D" rotation matrix"""
	return sp.Matrix([
		[sp.cos(angle), 	-sp.sin(angle), 	0],
		[sp.sin(angle), 	sp.cos(angle), 		0],
		[0, 				0, 					1]])

def getSPTranslationMatrix(x, y):
	"""Returns a homogenous "2D" translation matrix"""
	return sp.Matrix([
		[1,		0, 		x],
		[0, 	1, 		y],
		[0,		0,		1]])

def getRotationMatrix(angle):
	"""Returns a homogenous "2D" rotation matrix"""
	return np.matrix([
		[cos(angle), 	-sin(angle), 	0],
		[sin(angle), 	cos(angle), 	0],
		[0, 				0, 			1]])

def getTranslationMatrix(x, y):
	"""Returns a homogenous "2D" translation matrix"""
	return np.matrix([
		[1,		0, 		x],
		[0, 	1, 		y],
		[0,		0,		1]])

def getTransformationMatrix(angle, translationX, translationY):
	"""Returns the transformationMatrix of R(angle) * T(tx, ty)"""
	return getRotationMatrix(angle) * getTranslationMatrix(translationX, translationY)

def getSPTransformationMatrix(angle, translationX, translationY):
	"""Returns the transformationMatrix of R(angle) * T(tx, ty)"""
	return getSPRotationMatrix(angle) * getSPTranslationMatrix(translationX, translationY)


def drawPoint(point, label = "", plotThingy = plt):
	"""Draws a big dot on the plot at the given point coordinate, with a given label"""
	plotThingy.plot(point[0], point[1], "o", label = label)
	plt.show(block=False)
	plt.pause(0.001)

def drawArrow(point1, point2, plotThingy = plt):
	"""Draws an arrow pointing from point1 to point2"""
	displacement = (point2 - point1)
	plotThingy.arrow(float(point1[0]), float(point1[1]), float(displacement[0]),
		float(displacement[1]), fc = "k", ec = "k", head_width = 0.05, head_length = 0.1)
	plt.show(block=False)

def drawFrame(frameMatrix):
	"""Draws a frame with the origin and unit vectors multiplied by the given frame matrix"""
	startPoint = frameMatrix * np.matrix([0, 0, 1]).T
	x, y = frameMatrix * np.matrix([1, 0, 1]).T, frameMatrix * np.matrix([0, 1, 1]).T
	drawPoint(x, "X")
	drawPoint(y, "Y")
	drawPoint(startPoint, "Start")
	drawArrow(startPoint, x)
	drawArrow(startPoint, y)
	plt.show(block=False)
	plt.pause(0.001)

def angleWrap(angle):
	"""Returns a wrap-around calculation for an angle in radians... Makes sure
	that the following holds: 	-pi <= angle < pi"""
	twopi = 2.0 * pi
	angleOs = angle + pi
	angleOs = angleOs - twopi * floor(angleOs / twopi)
	return angleOs - pi

def pseudoinverse_r(m):
	"""Returns the pseudoinverse using the SVD method of numpy."""
	return np.dot(np.linalg.inv(np.dot(m.T, m)), m.T)

if __name__ == "__main__":
	# Setup subplots, one for the finger and one for the iterations vs error graph
	figure, ax = plt.subplots(1, 2)
	plt.subplots_adjust(bottom = 0.4)

	# Setup middle finger
	middleFinger = Finger([PROXIMAL_LINK, INTERMEDIATE_LINK, DISTAL_LINK], figure, ax[0])
	middleFinger.drawFinger()

	# Add an inverse kinematics solcer
	solver = InverseKinematicsSolver(middleFinger)

	# And finally we add the interaction gui for sliders and shit, below subplots
	gui = Interaction(middleFinger, solver, figure, ax)
	plt.show(block=False)
	plt.draw()
	gui.resetEverything()
	plt.show()

